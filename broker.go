package ssego


const (
    bufferSize = 10
)

type Broker interface {
    Subscribe() chan []byte
    Unsubscribe(chan []byte)
    Publish(msg []byte)
}

type simpleBroker struct {
    subscribers map[chan []byte]bool
}

func (b *simpleBroker) Subscribe() chan []byte {
    ch := make(chan []byte, bufferSize)
    b.subscribers[ch] = true
    return ch
}

func (b *simpleBroker) Unsubscribe(ch chan []byte) {
    delete(b.subscribers, ch)
}

func (b *simpleBroker) Publish(msg []byte) {
    for ch := range b.subscribers {
        ch <- msg
    }
}

func NewBroker() Broker {
    return &simpleBroker{make(map[chan []byte]bool)}
}

