package ssego

import (
    "fmt"
    "net/http"
)

func NewEventSource(broker Broker) http.HandlerFunc {
    return func(w http.ResponseWriter, _ *http.Request) {
        f, ok := w.(http.Flusher)
        if !ok {
            http.Error(w, "streaming is not supported", http.StatusInternalServerError)
            return
        }

        w.Header().Set("Content-Type", "text/event-stream")
        w.Header().Set("Cache-Control", "no-cache")
        w.Header().Set("Connection", "keep-alive")

        ch := broker.Subscribe()
        defer broker.Unsubscribe(ch)

        for {
            msg := <-ch
            fmt.Fprintf(w, "data: %s\n\n", msg)
            f.Flush()
        }
    }
}
